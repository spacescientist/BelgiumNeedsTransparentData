# BelgiumNeedsTransparentData

## Data used
The data are January and February new hospitalisation data from Sciensano, taken from their dashboard as of Friday February 26th `[1]`.

These as can also be recalculated via the historical records for 2021/02/26 `[2]`.

The data for the following days are directly taken from Sciensano on the release day (i.e. the following day); the same figures are released in the press.

#### For example
###### Friday 2021/02/26
204 new hospital admissions [3]
###### Saturday 2021/02/27
152 new hospital admissions [3]
###### Sunday 2021/02/28
127 new hospital admissions [4]

### References
* [1] https://datastudio.google.com/embed/reporting/c14a5cfc-cab7-4812-848c-0369173148ab/page/uTSKB (consulted on February 26th 2021)
* [2] https://epistat.sciensano.be/covid/covid19_historicaldata.html (Hospitalisations by date and provinces);
* [3] see, e.g. https://www.lesoir.be/357836/article/2021-02-28/coronavirus-les-hospitalisations-et-nouvelles-contaminations-toujours-en-hausse
* [4] see, e.g. https://www.rtbf.be/info/societe/detail_coronavirus-en-belgique-ce-1er-mars-la-flambee-des-admissions-se-poursuit-les-contaminations-vers-un-plateau?id=10708440
